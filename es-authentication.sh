# setup: ./config/elasticsearch.yml

discovery.type: single-node

xpack.security.enabled: true

cluster.routing.allocation.disk.threshold_enabled: true
cluster.routing.allocation.disk.watermark.flood_stage: 2gb
cluster.routing.allocation.disk.watermark.low: 3gb
cluster.routing.allocation.disk.watermark.high: 2gb

# set pass:
bin/elasticsearch-setup-passwords interactive

elastic

# start:
./bin/elasticsearch
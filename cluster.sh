# 1) Donwload/install:
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.2.0-linux-x86_64.tar.gz

# 2) Decompress:
tar -xzf elasticsearch-7.2.0-linux-x86_64.tar.gz
cd elasticsearch-7.2.0/ 

# 3) Set user for Elastic Search:
./bin/elasticsearch-users  useradd hpnuser -p hpnpass -r  superuser


# 4) Set config for Elastic Search (in ./config/elasticsearch.yml)
# Master:
echo "" >> ./config/elasticsearch.yml
echo "cluster.name: es-int"  >> ./config/elasticsearch.yml
echo "node.name: node-1"  >> ./config/elasticsearch.yml
echo "node.master: true"  >> ./config/elasticsearch.yml
echo "node.data: false"  >> ./config/elasticsearch.yml
echo "path.data: /tmp/elasticsearch.data"  >> ./config/elasticsearch.yml
echo "path.logs: /tmp/elasticsearch.log"  >> ./config/elasticsearch.yml
echo "network.host: _local_, _site_"  >> ./config/elasticsearch.yml
echo 'discovery.seed_hosts: ["10.25.242.167", "10.25.247.62" ]'  >> ./config/elasticsearch.yml
echo 'cluster.initial_master_nodes: ["10.25.242.167"]'  >> ./config/elasticsearch.yml
echo "http.port: 9200"  >> ./config/elasticsearch.yml
echo "transport.host: localhost"  >> ./config/elasticsearch.yml
echo "transport.tcp.port: 9300"  >> ./config/elasticsearch.yml

# Slave:
echo "" >> ./config/elasticsearch.yml
echo "cluster.name: es-int"  >> ./config/elasticsearch.yml
echo "node.name: node-2"  >> ./config/elasticsearch.yml
echo "path.data: /tmp/elasticsearch.data"  >> ./config/elasticsearch.yml
echo "path.logs: /tmp/elasticsearch.log"  >> ./config/elasticsearch.yml
echo "network.host: 0.0.0.0"  >> ./config/elasticsearch.yml
echo "http.port: 9200"  >> ./config/elasticsearch.yml
echo 'discovery.zen.ping.unicast.hosts: ["10.25.242.167" ]'  >> ./config/elasticsearch.yml
echo 'cluster.initial_master_nodes: ["10.25.242.167"]'  >> ./config/elasticsearch.yml
echo "transport.host: localhost"  >> ./config/elasticsearch.yml
echo "transport.tcp.port: 9300"  >> ./config/elasticsearch.yml

 
# 5) Start:
./bin/elasticsearch


# 6) Test:
curl localhost:9200  # (should NOT work)
curl localhost:9200 -u hpnuser:hpnpass # (should work)





### Master (do not specify network.bind_host: 172.31.27.57):
node.name: mymaster
cluster.name: mycluster
cluster.initial_master_nodes: ["172.31.21.144"]
transport.host: localhost
network.host: 172.31.21.144
# network.host: 0.0.0.0
discovery.zen.ping.unicast.hosts: ["172.31.21.144", "172.31.29.50" ]
network.publish_host: 172.31.21.144
http.port: 9200
transport.tcp.port: 9300
node.master: true


### Slave:
node.name: myslave
cluster.name: mycluster
cluster.initial_master_nodes: ["172.31.21.144"]
transport.host: localhost
network.host: 172.31.29.50
# network.host: 0.0.0.0
discovery.zen.ping.unicast.hosts: ["172.31.21.144", "172.31.29.50" ]
network.publish_host: 172.31.29.50
http.port: 9200
transport.tcp.port: 9300




### Test:
curl 172.31.21.144:9200/_cluster/health?pretty
curl 172.31.29.50:9200/_cluster/health?pretty



netstat -tulpn | grep 9
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
tcp6       0      0 :::9200                 :::*                    LISTEN      5755/java
tcp6       0      0 127.0.0.1:9300          :::*                    LISTEN      5755/java
udp6       0      0 fe80::c97:29ff:febc:546 :::*                                -



nmap -p 9200 172.31.27.57

Starting Nmap 6.40 ( http://nmap.org ) at 2019-07-16 10:39 UTC
Nmap scan report for ip-172-31-27-57.ec2.internal (172.31.27.57)
Host is up (0.00056s latency).
PORT     STATE SERVICE
9200/tcp open  wap-wsp

Nmap done: 1 IP address (1 host up) scanned in 0.03 seconds
[ec2-user@ip-172-31-27-57 ~]$ nmap -p 9300 172.31.27.57

Starting Nmap 6.40 ( http://nmap.org ) at 2019-07-16 10:39 UTC
Nmap scan report for ip-172-31-27-57.ec2.internal (172.31.27.57)
Host is up (0.000031s latency).
PORT     STATE  SERVICE
9300/tcp closed vrace

Nmap done: 1 IP address (1 host up) scanned in 0.03 seconds
